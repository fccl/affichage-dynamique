Atelier affichage dynamique du 18 juin 2022 à la Fabrique des possibles. Implémentation de vidéos dans l’affichage dynamique.

# Fichier html à générer

Pour introduire la lecture d'une vidéo depuis une instance Peertube à l’aide d’un Iframe.
L’exemple ci-dessous sera réutilisé pour générer un script qui génère le code automatiquement.

```
<!DOCTYPE html>
<html>
<head>
  <style>
  html,body {
    margin: 0px;
    padding: 0px;
    height: 100%;
  }
  </style>
</head>

<body>

<iframe marginheight="0" marginwidth="0" width="100%" height="100%" sandbox="allow-same-origin allow-scripts allow-popups" src="https://webtv.vandoeuvre.net/videos/embed/e6681a6f-61ec-4f16-bd77-abdb27d89ca6?autoplay=1&title=0&warningTitle=0&controls=0&peertubeLink=0" frameborder="0" allowfullscreen></iframe>

</body>

</html>
```

