
**Atelier affichage dynamique du 7 mai 2022 à la Fabrique des possibles. Mise en place du démarrage de Firefox automatiquement au démarrage de l'appareil**

# Mise en application

Mise en place des taches automatiques dans le crontab.

> Astuce : utiliser le site crontab.guru pour programmer l'heure

## Modifier crontab

    sudo crontab -e

=> Ouvre un fichier /tmp/crontab.{idenitifiantAuto}/crontab

**Lignes ajoutées**

    34 10 \* *\** \* /usr/sbin/shutdown now\
    @reboot /bin/sleep 120 ; /usr/bin/firefox -kiosk

> La commande se lance bien au demarrage d'apres les logs mais rien ne passe. Nous imaginons qu'il faut utiliser la commande l'option `DISPLAY=:0.0` dans la commande. Cela fait planté le serveur d'affichage.
> Les personne présentes ont tenté de réanimer le système via diverses manipulations l’enfonçant toujours d'avantage dans les ténèbres, jusqu'au fond des abysses...
> Bref ! est proposé de tester une autre solution pour le prochain atelier

## Autre solution

> Utiliser l'outil proposé par l'interface graphique du RPI

Source des manips à étudier :

<https://www.makeuseof.com/how-to-run-a-raspberry-pi-program-script-at-startup/>
