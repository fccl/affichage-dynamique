**Atelier affichage dynamique du 16 avril 2022 à la Fabrique des possibles. Tests sur les démarrages et extinctions automatiques des logiciels.**

# Alimentation

Programmeur électronique pour couper l’alimentation en fin d’affichage quelques minutes après une extinction logicielle propre via _crontab_.
Remise de l’alimentation, allumage automatique du Raspberry Pi, démarrage de Firefox via _crontab_.

# Crontab 

## Aide

Est suggéré l'usage de l'outil en ligne [crontab.guru](https://crontab.guru) pour faciliter la création de lignes sur _crontab_.

## Problème rencontré

`@reboot` ne semble pas fonctionner (Firefox ne se lance pas).

Après quelques recherches sur Internet, il apparaît qu’il faut démarrer l’édition du fichier _crontab_ à l’aide de la commande _crontab -e_. Celle-ci proposera ensuite différents éditeurs de texte.

## Manipulations à réaliser

**_crontab -e_ pour modifier le fichier cron**

> Ajouter pour éteindre logiciel le Rpi proprement (à adapter, dans le cas présent il s’éteint tous les jours à 22 h)

`00 22 * * * root shutdown now`

**Démarrer automatiquement Firefox en mode Kiosk**

> 60 correspond à 60 secondes soit une minute.

`@reboot root /bin/sleep 60 ; firefox -kiosk`

## Résultat

Nous avons planté le Raspberry Pi OS suite à d’autres tests, nous poursuivrons les manipulations précédentes lors du prochain atelier. Le Raspberri Pi OS sera réinstallé pour l'occasion.
