**Atelier affichage dynamique du 12 mars 2022 à la Fabrique des possibles. Étude de différentes solutions.**

# Affichage

## Solution 1 : config sur le Raspberry Pi

- Démarrer Firefox en mode kiosk
- Lancer extension Tab Rotator qui change automatiquement les onglets qui charge des pages

**Problématiques**

- Coupure de courant mode kiosk : crontab @reboot avec éventuellement un sommeil (pour laisser le système démarrer proprement
- Coupure de courant onglets : Tab Rotator permet de démarrer au démarrage du navigateur (vu sur description).
- Comment gérer les onglets si en mode kiosk ?

## Solution 2 : Gestion sur service web Javascript

- Démarrer Firefox en mode kiosk
- Questionner un page web unique, qui affiche des infos via Iframe
- Créer un programme Javascript sur le client
-Développer une interface web sur un serveur à part dans laquelle on insère les pages web à afficher. Gestion de compte, activer - désactiver des infos.

**Problématiques**

- Que se passe-t-il si image dans une Iframe ?
- Est-ce que des sites bloques les Iframe pour des raisons de sécurité ?

## Solution 3 : Gestion sur service web Python

- Démarrer Firefox en mode kiosk
- Python peut simuler le clavier. Pas d'Iframe, Python charge la page web.
- Développer une interface web sur un serveur à part dans laquelle on insère les pages web à afficher. Gestion de compte, activer - désactiver des infos.
- Proposer dans l'interface web plusieurs possibilités : charger une page web, changer une image téléversé sur le serveur, charger une vidéo sur Peertube en auto-lancement en indiquant uniquement, et le référencement.

**Problématiques**

- temps de transition par rapport avec une vidéo ?
- cache de la vidéo : permettre de ne pas recharger à chaque diffusion la vidéo.
- si la vidéo n'est pas sur Peertube Vandœuvre ? Peut être récupérer le lien du nom de domaine pour utiliser d'autres Peertube ?

# Contrôle à distance

## Solution 1 : VNC

- Installer un serveur VNC sur le Raspberri Py
- Installer un client VNC sur le poste qui contrôlera le RPy

**Problématiques**

- Nécessite de gérer le réseau (ouvrir port, fixer l'IP, etc)
- Être administrateur système sur Windows, pas de client natif. Pas vraiment de client portable.
Passer par UDP ? Non protocole spécifique aux systèmes Windows, RPy tournera avec Linux.

## Solution 2 : DWServices

- Installer un client sur sur le Raspberry Pi qui est référencé.
- Pas de client sur le pc qui contrôle. Ça passe par un site web via un compte à créer.

**Problématiques**

- Le client et les api sont libres, le service en ligne non. Toutefois le projet semble « vertueux ».
- Quid si le service ferme ?