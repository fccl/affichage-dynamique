# Création d'une station d'affichage

## Préparer un Raspberry pi

Procéder au téléchargement puis préparation de la carte micro sd du Raspberry Pi. Choisissez de préférence le système Raspberry PI OS en version **Raspberry Pi OS with desktop**.\
Voici ici https://www.raspberrypi.com/software/

*La procédure de préparation de la carte et d'installation du système ne sont pas décrits dans cette documentation.*

## Installer Firefox

Par défaut c'est Chromium qui est proposé. Pour profiter du module complémentaire et utiliser le mode Kiosk proposés dans cette documentation, installez Mozilla Firefox ESR.

    sudo apt install firefox-esr firefox-esr-l10n-fr

## Configurer Tab Rotator

Depuis Firefox, installez l'extension **Tab Rotator**\
https://addons.mozilla.org/fr/firefox/addon/tab-rotator/

Lorsque installé, un bouton dans la barre d’outils de Firefox apparaît, faites clic droit puis **Gérer l'extension**.

### Temps d'affichage

Onglet **Préférences**, indiquez un temps de rotation pour l'ensemble des onglets via le champ **Rotation Time**. Par exemple si 10 secondes, écrire 10.

Dans le cas que vous souhaitez configurer des temps différents selon les onglets, écrire le nombre de secondes souhaité selon le nombre d'onglets ouverts.

> Par exemple, diffusion d'une
> 
> - page web pendant 10 secondes dans le premier onglet,
> - vidéo de 59 secondes dans le second onglet,
> - image pendant 3 secondes sur le dernier onglet.
> 
> -> Indiquez dans le champ **Rotation Time** : **10;59;3**

Remarque : Si la grande majorité des onglets on un temps similaire, mettez un média sur le premier onglet ayant besoin de ce temps classique, sur les onglets suivant les médias ayant besoin de temps atypiques, puis pour terminer l'ensemble des autres média sans précision de temps. Ces derniers se baserons sur le temps du premier onglet.

### Autres options

Toujours dans l'onglet **Préférences** de l'extension **Tab Rotator**, voici la configuration conseillé :

- [ ] Reload the current tab, after it has been displayed.

> Cela afin d'éviter de surcharger la machine et le réseau inutilement. Toutefois Firefox sera configuré pour que le démarrage journalier recharge l’entièreté des contenus.

- [x] Start Tab Rotator automatically when the browser starts.

> Pour que le démarrage journalier de l’affichage soit totalement indépendant d'intervention humaine.

- [x] Rotate the tabs only in the initially active window
- [ ] Rotate the tabs only in the currently active window
- [ ] Rotate the tabs in all windows simultaniously

> Le Raspberry Pi est assez léger coté performance, inutile de le charger via plusieurs sessions...

## Configurer Firefox

### Purger à la fermeture

> Si des pages dynamiques sont utilisées, cela permet chaque jour de réinitialiser le contenu et donc afficher les nouveautés.

Dans **Paramètres**, onglet **Vie privée et sécurité**, rubrique **Cookies et données de sites**, cochez la case **Supprimer les cookies et les données des sites à la fermeture de Firefox**.

### Affichage par défaut

> Cette manip est conseillé à réaliser après chaque mise à jour d'onglets pour assurer le redémarrage sur ceux-ci.

Dans **Paramètres**, onglet **Accueil**, rubrique **Nouvelles fenêtres et nouveaux onglets**, option Page d’accueil et nouvelles fenêtres,\
sélectionnez dans le menu déroulant **Adresses web personnalisées...** puis **Pages courantes**.

## DWService sur Raspberry pi

### Python

> Manip nécessaire en ce jour du 2 avril 2022, le client DWService n’étant pas compatible avec Python 3.

#### Installer Python 2.7

    sudo apt install python2.7

#### Accès à Python 2

Supprimer le lien symbolique vers Python 3 et remplacer par Python 2

    sudo unlink /usr/bin/python
    sudo ln -s /usr/bin/python2.7 /usr/bin/python

### Installer DWService

Se rendre sur la page des clients et télécharger celui pour **Raspberry**.\
https://www.dwservice.net/fr/download.html

#### Rendre le script exécutable

> Après s’être rendu dans le dossier Téléchargements

    cd Téléchargements
    chmod +x dwagent.sh

#### Lancer le script

    sudo bash dwagent.sh

#### Suivre cette procédure

- **Installer**, **Suivant**.
- Laisser le **Chemin** par défaut, **Suivant**.
- **Oui**, **Suivant**.
- **Entrez le code d'installation**, **Suivant**.
- Indiquez le **Code** sous ce format : 123-456-789, **Suivant**.

> Le code est indiqué par le service DWService, non détaillé sur cette documentation.

- **Fermer**.

## Mode Kiosk Firefox

### Manipulations de base

Commande pour démarrer en mode kiosk

    firefox -kiosk

Quitter le mode kiosk

`Alt + F4`

### Démarrage automatique

Créer le fichier firefox.desktop.

    sudo nano /etc/xdg/autostart/firefox.desktop

Entrer les informations ci-dessous :

```
[Desktop Entry]
Name=Firefox
Exec=firefox -kiosk
```

Enregistrer ce que vous avez mis et quitter.

    Ctrl + x, o, Entrée

### Extinction automatique

#### Ouvrez le fichier crontab

> Le fichier crontab est modifié pour lancer Firefox automatiquement au démarrage de l'ordinateur, et eteindre proprement le Raspberry Pi avant coupure éléctrique.

    sudo crontab -e

Puis sélectionnez la première option : ouvrir avec nano (1, Entrée).

#### Modifiez le fichier crontab

Ajouter cette ligne à la fin du fichier, puis enregistrez et fermez (Ctrl + x, o, Entrée).

> Dans cet exemple, le Raspberry Pi s'éteint chaque jour à 22 h.

`00 22 * * * /usr/sbin/shutdown now`

Redémarrer le Raspberry-Pi.

    sudo reboot
